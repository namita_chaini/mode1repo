package day1assign;

import java.util.Scanner;

public class swap {

	public static void main(String[] args) {
		int x,y,t;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter no. ");
        x=sc.nextInt();
        y=sc.nextInt();
        System.out.println(" before swapping= "+x+" "+y);
        t=x;
        x=y;
        y=t;
        System.out.println(" after swapping="+x+" "+y);
	}

}

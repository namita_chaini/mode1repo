package day2assignment;

import java.util.Scanner;

public class MiddleCharacter {
	
	public static void main(String[]args)
	{
		System.out.println("Enter the string: ");
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		int n=str.length();
		int t=n/2;
		if(n%2==0) {
			
			System.out.println("Middle characters of string : "+str.charAt(t-1)+str.charAt(t));
			
			
			
		}
		else {
			System.out.println("Middle character is: "+str.charAt(t));
			
		}
		
	}

}

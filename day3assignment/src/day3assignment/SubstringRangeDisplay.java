package day3assignment;

import java.util.Scanner;

public class SubstringRangeDisplay {
	public static void main(String[]args) {
		int a,b;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string: ");
		String str=sc.nextLine();
		System.out.println("Enter the range : ");
		a=sc.nextInt();
		b=sc.nextInt();
		
		String strRange=str.substring(a, b);
		System.out.println("Substring range from"+a+"to"+b+" : "+strRange);
		
	}

}
